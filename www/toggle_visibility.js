// Get all the sections
const sections = document.querySelectorAll('section');

// Loop through the sections and get the date element
sections.forEach((section) => {
  const dateToggle = section.querySelector('p:first-child');
// Add click event listener to each date toggle
dateToggle.addEventListener('click', (event) => {
    // Toggle the active class on all sections
    sections.forEach((section) => {
      section.classList.toggle('do-show-me');
    });
});
});
 
