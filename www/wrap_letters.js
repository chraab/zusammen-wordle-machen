// take any element of class letter-box
// put letters of its text into boxes
function wrapLettersInButtons(str, grid) {
    const letters = str.split('');
    const boxes = letters.map(letter => {
      return `<div class="letter-box">${letter}</div>`;
    });
    grid.innerHTML = boxes.join('');
  }
  
  const grids = document.querySelectorAll('.letter-row');
  
  grids.forEach(grid => {
    const inputString = grid.innerHTML;
    wrapLettersInButtons(inputString, grid);
  });