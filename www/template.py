import sys, os
from datetime import date
import logging
import locale
logging.basicConfig(level=logging.WARNING)
locale.setlocale(locale.LC_TIME, ['en_GB', 'UTF-8'])

# all paths are relative to the web directory
web = sys.argv[1]
templatepath = os.path.join(web, 'index.template')
datapath = os.path.join(web, 'swotd.txt')
outpath = os.path.join(web, 'index.html')

# load the template and raw data
template = open(templatepath).read()
data_raw = open(datapath).readlines()

# data will be a list containing [date, SWOTD, backup word]
data = []

for i, line in enumerate(data_raw):
    line = line.split(':')[1].strip().upper()
    if i%3==0:
        # add the weekday to the date
        date_object = date(*(int(_) for _ in line.split('-')))
        data.append(date_object.strftime('%A %Y-%m-%d'))
    else:
        data.append(line)

logging.info(str(data))

# fill in template to write the new page
out = template.format(*(_ for _ in data))

open(outpath, 'w').write(out)

