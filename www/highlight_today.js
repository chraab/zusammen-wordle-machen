function updateHighlight() {
// Get the current date
const currentDate = new Date();
const currentYear = currentDate.getFullYear();
const currentMonth = String(currentDate.getMonth() + 1).padStart(2, '0');
const currentDay = String(currentDate.getDate()).padStart(2, '0');
const currentDateString = `${currentYear}-${currentMonth}-${currentDay}`;

// Get all the sections
const sections = document.querySelectorAll('section');

// Loop through the sections and check the date
sections.forEach((section) => {
  const sectionDateElement = section.querySelector('p:first-child');
  // Extract the date part from the section date element
  const sectionDateString = sectionDateElement.textContent.trim().split(' ')[1];

  if (sectionDateString.includes(currentDateString)) {
    section.classList.add('current-date');
    section.classList.remove('not-current-date');
  } else {
    section.classList.add('not-current-date');
    section.classList.remove('current-date');
  }
});

setTimeout(updateHighlight, 1000);
};

updateHighlight();