#!/bin/bash

# set paths to act on
webpath="${HOME}/swotd/"

# get rest relative to script location
shellscript=$(realpath $0)
sourcepath=$(dirname $shellscript)
packagepath=$(dirname $sourcepath)
scriptpath="${packagepath}/zusammen_wordle_machen/swotd.py"
datapath="${sourcepath}/swotd.txt"

# venv with the package installed
source "${packagepath}/venv/bin/activate"

# write SWOTD data to txt file
python3 ${scriptpath} -o -1 > ${datapath}
python3 ${scriptpath} -o 0 >> ${datapath}
python3 ${scriptpath} -o 1 >> ${datapath}

# render webpage from template
python3 "${sourcepath}/template.py" "${sourcepath}"

# deploy files
if [ -d ${webpath} ];
then {
cp -R ${sourcepath}/. "${webpath}/"
}; fi
