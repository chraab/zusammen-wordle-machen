import multiprocessing
import argparse
from zusammen_wordle_machen.dictionary import legal, validate


def levenshtein_one(a, b):
    '''Check if strings a and b have a Levenshtein distance of 1.
    Special case for strings of equal length.'''
    assert len(a)==len(b)
    distance = 0
    for _a, _b in zip(a, b):
        if _a != _b:
            distance += 1
        if distance > 1:
            return False
    if distance==1:
        return True


def count_neighbours(s, words=legal):
    return sum([1 if levenshtein_one(_s, s) else 0 for _s in words])

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Look at connectedness of a word')
    parser.add_argument('word', type=str,
        help='Input word, must match wordle dictionary format',
    )

    args, unknown = parser.parse_known_args()
    validate(args.word)
    if not args.word in legal:
        print(f'{args.word} is not in the dictionary, interpret carefully')

    p = multiprocessing.Pool(8)
    neighbours = p.map(count_neighbours, [args.word] + legal)
    print(f'{args.word} has {neighbours[0]} Levenshtein neighbours')

    higher = sum(_n>neighbours[0] for _n in neighbours[1:])
    quantile = higher/len(legal)

    print(f'{quantile:.2%} of words ({higher}/{len(legal)}) have more')
