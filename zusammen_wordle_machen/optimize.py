import string
from zusammen_wordle_machen.dictionary import validate

def letter_frequency(w):
    '''Record frequency of letters in word list `w`
    '''
    n = {_l:0 for _l in string.ascii_lowercase}
    for _w in w:
        for _c in n.keys():
            if _c in _w:
                n[_c]+=1
    return n


def score_function(word, frequency, excluded=''):
    '''Score `word` by the frequency sum of its letters,
    counting each only once and skipping `excluded`.
    '''
    score = 0
    for _c in string.ascii_lowercase:
        if _c in word and not _c in excluded:
            score += frequency[_c]
    return score


def rank_words(words, excluded='', n=10):
    '''Rank all `words` with score_function, assuming you have already
    covered all letters in `excluded`, return `n` best words.'''
    return sorted(words, key=lambda _w:score_function(_w, excluded=excluded))[-n:]
