import string
import os

# dictionary files are in a directory
# relative to the location of this module
module_dir = os.path.dirname(__file__)
data_dir = os.path.join(module_dir, 'dictionaries')

def load(filename):
    '''Load dictionary from `filename` into a list.
    '''
    with open(filename,'r') as f:
        words = f.readlines()
    words = [_w[1:-3] for _w in words]

    for _w in words:
        try:
            validate(_w)
        except:
            raise ValueError('Check your input dictionary')

    words.sort() # remove any hints
    return words


def validate(word):
    '''Validate `word` is a lower-case five-letter string.
    '''
    if len(word)!=5:
        raise ValueError(f'word `{word}` is not 5 letters')
    if not all(_c in string.ascii_lowercase for _c in word):
        raise ValueError(f'word `{word}`` is not all letters')



legal = load(os.path.join(data_dir, 'wordle_legal.txt'))
