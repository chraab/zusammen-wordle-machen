#!/usr/bin/env python

import argparse
import random
from datetime import date, timedelta
from zusammen_wordle_machen.dictionary import legal


parser = argparse.ArgumentParser(description='Choose a reproducibly random starting word for today.')
parser.add_argument('--offset', '-o', type=int, default=0,
    help='Travel backwards/forwards by [OFFSET] days.',
)

def main(args):
    day = date.today() + timedelta(days=args.offset)
    candidates = legal
    random.seed(day.toordinal())

    print('Date:', day)
    print('Starting word:', random.choice(candidates))
    print('Backup word:', random.choice(candidates))

if __name__=='__main__':
    args, unknown = parser.parse_known_args()
    main(args)
