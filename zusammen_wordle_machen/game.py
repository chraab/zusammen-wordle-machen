import argparse
import math
import sys
from zusammen_wordle_machen.dictionary import legal, validate

class Hint:
    def __init__(self, hint:list):
        self.hint = hint
        self.word = ''.join(h[0] for h in self.hint)
        self.colors = ''.join(h[1] for h in self.hint)
        hint_dict = {}
        for c in 'byg':
            hint_dict.setdefault(c, [])
        for i, (g,c) in enumerate(self.hint):
            hint_dict[c].append((i,g))
        self.green = hint_dict['g']
        self.yellow = hint_dict['y']
        self.black = hint_dict['b']



def evaluate(guess, solution):
    '''Assuming solution, return the black, yellow and green letters of a guess.
    Each is return as a string where `_` is the placeholder.
    '''
    color = []
    count_in_guess = {}
    count_in_solution = {}
    for i, (g,s) in enumerate(zip(guess, solution)):
        if g==s:
            color.append('green')
            continue
        if g not in solution:
            color.append('black')
            continue
        # guess[i] could be yellow, as it occurs in the solution
        # but not at that place
        color.append('maybe yellow')
        # but it depends on how many times it was in the guess and solution respectively
        # we are only counting 'maybe yellow' letters and the matching place in the solution
        # if it was green, no need to count the letter in the guess OR in the solution!
        count_in_guess.setdefault(g, 0)
        count_in_guess[g] += 1

    # for all maybe-yellow letters
    for letter, count in count_in_guess.items():
        count_in_solution[letter] = 0
        # count how often it occurs in the solution, except the places we already found
        for i, s in enumerate(solution):
            if color[i] == 'green':
                continue
            if s==letter:
                count_in_solution[letter] += 1
        # now we have to mark the maybe-yellow letters in the guess
        # - yellow: as many as there are in the solution
        # - black: the rest
        n_marked_yellow = 0
        for i,g in enumerate(guess):
            if g!=letter:
                continue
            if n_marked_yellow < count_in_solution[letter]:
                color[i] = 'yellow'
                n_marked_yellow += 1
            elif color[i] == 'green':
                continue
            else:
                color[i] = 'black'
    return Hint([(g, c[0]) for  g, c in zip(list(guess), color)])

def select(hint, starting_selection=legal):
    '''Pare down `starting_selection` testing against the `hint` of black, yellow and green letters.
    '''
    # green is easy to apply: exact match
    sel = []
    for word in starting_selection:
        if test(hint, word):
            sel.append(word)
    return sel

def test(hint, word):
    '''Return True if `word` is compatible with a `hint` of black, yellow and green letters.'''
    return evaluate(hint.word, word).colors == hint.colors


def game(solution=None, threshold=2, allow_all=False):
    '''Repeat a game of wordle for `solution`
    '''
    if not solution in legal:
        print(f'{solution} is not a legal solution')
        return

    sel = legal

    # count digits to keep prompts aligned but tight
    digits = int(math.log10(len(sel)))+1

    while len(sel)>1:
        if len(sel)<=threshold:
            print(' '.join(sel))
        # read input guess or exit programme if desired
        try:
            guess = input(f'{len(sel):{digits}d} left, guess: ')
        except EOFError: # user has hit Ctrl+D etc.
            sys.exit('\n')
        except KeyboardInterrupt: # user has hit Ctrl+C 
            sys.exit('\n')
        guess = guess.lower().strip()
        try:
            validate(guess)
        except Exception as e:
            print(e)
            continue
        if (not guess in legal) and (not allow_all):
            print('not a legal guess')
            continue
        sel = select(evaluate(guess, solution), starting_selection=sel)
    else:
        if guess!=solution:
            print(f'{len(sel):{digits}d} left, must be', sel[0])

parser = argparse.ArgumentParser(description='Play wordle knowing the solution')
parser.add_argument('solution', type=str,
    help='solution of the game to analyze',
)
parser.add_argument('--threshold', '-t', type=int, default=2,
    help='show possible words if no more than [threshold] are left',
)
parser.add_argument('--allow_all', '-a', action='store_true',
    help='Allow all 5-letter strings, not just those from the Wordle dictionary',
)

def main():
    args, unknown = parser.parse_known_args()
    return game(**args.__dict__)
if __name__=='__main__':
    main()