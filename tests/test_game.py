import unittest

class GameTest(unittest.TestCase):

    testgames = \
    [
        ('lunch', 'bunch', 'bgggg'),
        ('hunch', 'bunch', 'bgggg'),
        ('lucky', 'bunch', 'bgybb'),
        ('funny', 'bunny', 'bgggg'),
        ('bully', 'lunch', 'bgybb'),
        ('about', 'would', 'bbyyb'),
        ('hemes', 'spent', 'bybby'),
        ('laser', 'spent', 'bbyyb'),
        ('douse', 'spent', 'bbbyy'),
        ('speck', 'spent', 'gggbb'),
        ('click', 'quick', 'bbggg'),
        ('fairs', 'these', 'bbbby'),
        ('jerky', 'hosue', 'bybbb'),
        ('steal', 'house', 'ybybb'),
        ('dunes', 'house', 'bybyy'),
        ('abcde', 'abcde', 'ggggg'),
        ('aaaaa', 'aaaaa', 'ggggg'),
        ('aaaab', 'baaaa', 'ygggy'),
        ('abcde', 'fghij', 'bbbbb'),
        ('ratio', 'chain', 'bybgb'),
        ('steal', 'sleep', 'gbgby'),
        ('gobis', 'resin', 'bbbgy'),
        ('lucky', 'fully', 'ygbbg'),
        ('apart', 'adapt', 'gygbg'),
        ('rabis', 'genre', 'ybbbb'),
        ('where', 'genre', 'bbygg'),
        ('merry', 'genre', 'bgbgb'),
        ('fauna', 'taken', 'bgbyb'),
        ('lanes', 'taken', 'bgygb'),
        ('naive', 'taken', 'ygbby'),
    ]
    def test_evaluate(self):
        from zusammen_wordle_machen.game import evaluate
        for (guess, solution, hint) in self.testgames:
            with self.subTest(guess=guess, solution=solution, hint=hint):
                self.assertEqual(evaluate(guess, solution).colors, hint)

    def test_select(self):
        from zusammen_wordle_machen.game import evaluate, select
        from zusammen_wordle_machen.dictionary import legal

        sel = select(evaluate('lurch', 'bunch'), starting_selection=legal)
        n = len(sel)

        for guess, delta in [
            ('quick', 2),
            ('punch', 7),
            # munch: m is just one starting letter
            ('munch', 1),
            # mulch: both m and l were already excluded, options same
            ('mulch', 0),
            # hunch: it should know there's no h in first place, only 1 h in total
            # => hunch is excluded
            ('hunch', 1),
                             ]:
            sel = select(evaluate(guess, 'bunch'), starting_selection=sel)
            self.assertEqual(len(sel), n - delta)
            n = len(sel)

if __name__ == '__main__':
    unittest.main()