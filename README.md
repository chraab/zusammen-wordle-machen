
# zusammen-wordle-machen 🦦🦡🐻

Tools to analyze (official) wordle games, compare stategies, and pick starting words. Using nothing beyond the Python 3 Standard Library, just for fun. It will not work with Python 2 because come on, seriously?


## Installation
```
pip install git+https://gitlab.com/chraab/zusammen-wordle-machen.git
```

to get just the module and `wordle` script, or

```
git clone git@gitlab.com:chraab/zusammen-wordle-machen.git
cd zusammen-wordle-machen
python3 -m venv venv
source venv/bin/activate
pip install -e .
```

to retain e.g. the `www` directory to maintain the SWOTD webpage.

## Usage
## game.py
```
usage: game.py [-h] [--threshold THRESHOLD] [--allow_all] solution

Play wordle knowing the solution

positional arguments:
  solution              solution of the game to analyze

options:
  -h, --help            show this help message and exit
  --threshold THRESHOLD, -t THRESHOLD
                        show possible words if no more than [threshold] are left
  --allow_all, -a       Allow all 5-letter strings, not just those from the Wordle dictionary
```

Example:
```
$ python game.py brisk
2315 left, guess: tired
  46 left, guess: ducky
   6 left, guess: sleep
brisk frisk
   2 left, guess: frisk
1 left, must be brisk
```

## levenshtein.py
```
levenshtein.py [-h] word

Look at connectedness of a word

positional arguments:
  word        Input word, must match wordle dictionary format

optional arguments:
  -h, --help  show this help message and exit
```

Example:
```
$ python levenshtein.py parer
parer has 22 Levenshtein neighbours
2.56% of words (332/12972) have more
```

## optimize.py
to be implemented, now just a module


## swotd.py
Enables the extra-fun game mode of giving a new starting word each day! The words are randomly generated but completely determined by the date, so you can play with others. See it working at [the website](https://chraab.me/swotd) or run it yourself.

```
usage: swotd.py [-h] [--offset OFFSET]

Choose a reproducibly random starting word for today.

options:
  -h, --help            show this help message and exit
  --offset OFFSET, -o OFFSET
                        Travel backwards/forwards by [OFFSET] days.
```

Example:
```
$ date -I
2023-01-28
$ python swotd.py -o 1
Date: 2023-01-29
Starting word: aegis
Backup word: orcas
```


## License
[GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)
